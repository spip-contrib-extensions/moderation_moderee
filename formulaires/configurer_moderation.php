<?php

/**
 * Les saisies du formulaire de config
 **/
function formulaires_configurer_moderation_saisies_dist() {
	$saisies = [];
	foreach (['0minirezo', '1comite', '6forum'] as $valeur) {
		$saisies[] = [
			'saisie' => 'radio',
			'options' => [
				'nom' => $valeur,
				'label' => _T("moderation:{$valeur}_config"),
				'obligatoire' => true,
				'data' => [
					'on' => _T('item_oui'),
					'' => _T('item_non')
				]
			]
		];
	}
	return $saisies;
}

<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


include_spip('inc/config');
include_spip('inc/session');

function moderation_pre_edition($flux) {
	if (
		$flux['args']['table'] == 'spip_forum'
		and $flux['args']['action'] == 'instituer'
		and
		!in_array($flux['data']['statut'], ['prive','privrac','privadm'])
	) {
		// Pour publier direct les auteurs configur�s par mod�ration mod�r�e
		if ($GLOBALS['visiteur_session'] ?? '') {
			if (lire_config('moderation/' . ($GLOBALS['visiteur_session']['statut'] ?? '')) == 'on') {
				$flux['data']['statut'] = 'publie';
			}
		}
	}
	return $flux;
}
